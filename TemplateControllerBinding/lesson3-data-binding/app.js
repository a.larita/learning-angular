// Self invoke function to prevent exposing data object on the outside world.. :) COOL!
(phoneApp => {

    // phoneApp = angular.module('phoneApp', []); 

    let phones = [
        {
            name: 'iPhone 4',
            snippet: 'So Small but people still buy it because it\'s apple',
            age: 6
        },
        {
            name: 'iPhone 9',
            snippet: 'Sorry Dude You have been forgotten!',
            age: 3
        },
        {
            name: 'iPhone X',
            snippet: 'it is not X it is 10!!!',
            age: 1
        }
    ];

    /**
     * In the lesson says it should be extracted to its own js file,
     * For now I will just put this on one file since it is 
     * still a simple app and no complexity required.
     */
    phoneApp.component('phoneList', {
        templateUrl: 'phone-list/phone-list.template.html',

        controller () {
            this.phones = phones;
            this.orderBy = 'age';
        }
    });


    let myF = function (ref) {
        // All codes are guilty until proven innocent... 
        if (ref != 'a') {
            return;
        }

        return 'value';
    }

})( angular.module('phoneApp', []) ); // Looks weird.. XD