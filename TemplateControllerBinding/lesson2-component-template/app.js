// Self invoke function to prevent exposing your object on the outside world.. :) COOL!
(phoneApp => {
    
   phoneApp = angular.module('phoneApp', []);

   let phones = [
        {
            name: 'iPhone 4',
            snippet: 'So Small but people still buy it because it\'s apple'
        },
        {
            name: 'iPhone 9',
            snippet: 'Sorry Dude You have been forgotten!'
        },
        {
            name: 'iPhone X',
            snippet: 'it is not X it is 10!!!'
        }
    ];

    phoneApp.component('phoneList', {
        templateUrl: 'phone-list/phone-list.template.html',

        controller () {
            this.phones = phones;
        }
    })
 

})();