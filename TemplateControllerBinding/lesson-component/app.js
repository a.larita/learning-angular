/**
 * Angular Training: Components
 * Templates & Controller
 */

let app = angular.module('phoneApp', []);

/**
 * Assuming these are from the AJAX Request
 * Phone Collection
 */
let phones = [
    {
      name: 'iPhone 4',
      snippet: 'So Small but people still buy it because it\'s apple'
    },
    {
      name: 'iPhone 9',
      snippet: 'Sorry Dude You have been forgotten!'
    },
    {
      name: 'iPhone X',
      snippet: 'it is not X it is 10!!!'
    }
];

// Register <phone-list> component with template and it's controller
app.component('phoneList', {
    template: `
        <ul>
            <li ng-repeat="phone in $ctrl.phones">
              <span>{{ phone.name }}</span>
              <p>{{ phone.snippet }}</p>
            </li>
        </ul>
    `,

    controller () {
        this.phones = phones;
    }
});
