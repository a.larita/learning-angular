/*Define Module app*/
angular.module('app', []).controller('mainController', function () {
    this.message = 'Learning Angular...';

    /**
     * Update Message
     *
     * ES2016 shorthand syntax...
     *
     * @param  {String} message
     * @return {any}
     */
    this.changeMessage = message => this.message = message;
});
