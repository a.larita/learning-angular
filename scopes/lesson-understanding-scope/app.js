(app => {
    app = angular.module('app', []);

    /**
     * the $scope of the root is available on the entire application
     */
    app.run($rootScope => $rootScope.color = 'yellow');

    /**
     * if the child $scope has same properties {color} as the parent $scope
     * the application uses the one in the current $scope.
     *
     * Scope are controller specific. The child controller
     * inherits the scope of its parent controller;
     */
    app.controller('mainController', $scope => $scope.color = 'red yellow');

    app.controller('childController', $scope => $scope.color = 'red yellow blue');
})();
