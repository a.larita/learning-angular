'use strict';

(app => {

    /**
     * Define module bookApp
     * Inject bookApp.controllers
     */
    app = angular.module('bookApp', ['bookApp.controllers']);

    // .run() gets triggered when all the modules are loaded.
    app.run($rootScope => {
        $rootScope.title = 'Frontend Framework Books';
        $rootScope.name = 'Root Scope';
    });

})();