(module => {
    
    // Define bookApp.controllers module
    module = angular.module('bookApp.controllers', []);

    module.controller('SiteController', $scope => {
        $scope.publisher = 'SitePoint';
        $scope.type = 'Web Development';
        $scope.name = 'Scope for SiteController';
    });

    module.controller('BookController', $scope => {
        $scope.books = ['Angular 5 + TypeScript', 'Vue 2.*', 'React JS', 'SASS/LESS'];
        $scope.name = 'Scope for BookControlller';
    });

})()