let app = angular.module('app', []);

app.controller('AppController', ($scope, $http) => {
    $http.get('data.json').then(response => $scope.developers = response.data);
});
