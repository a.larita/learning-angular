let app = angular.module('app', []);

// Injecting $scope and $http
app.controller('AppController', ($scope, $http) => {
    // Let's load our announcements
    $http.get('announcements.html').then(response => $scope.announcements = response.data);
});

// since ng-bind-html-unsafe is deprecated from v1.2 up
// we create a custom directive to load unsafe html 
// from the ajax call
app.directive('bindUnsafeHtml',
    $compile => (scope, element, attrs) => {
        scope.$watch(
            scope => scope.$eval(attrs.bindUnsafeHtml),
            value => {
                element.html(value);
                $compile(element.contents())(scope);
            }
        );
    });
