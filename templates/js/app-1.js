// Define the Module
let app = angular.module('PhoneCatApp', []);

// Hardcoded the Collection for the List of Phones
// In Real life this data should be fetch via AJAX Request..
let phones = [
    {
      name: 'iPhone 4',
      snippet: 'So Small but people still buy it because it\'s apple'
    },
    {
      name: 'iPhone 9',
      snippet: 'Sorry Dude You have been forgotten!'
    },
    {
      name: 'iPhone X',
      snippet: 'it is not X it is 10!!!'
    }
];

/**
 * Here we declared a controller called PhoneListController and registered it in an AngularJS module, PhoneCatApp.
 * Notice that our ngApp directive (on the <html> tag) now specifies the PhoneCatApp module name as
 * the module to load when bootstrapping the application
 */
app.controller('PhoneListController', $scope => {
	// $scope.phones = phones;
    Object.assign($scope, { phones });

    // This kind of logic is best placed in computed property
    $scope.phoneCount = () => phones.length;

    /**
    * Pushing new Item to see if the DOM is updating and
    * the phoneCount also iterates to 1
    */
    phones.push({
        name: 'iPhone 11',
        snippet: 'Probably just a small iteration...'
    })

    /**
     * Add new Phone on the Phones Collection
     */
    $scope.add = () => {
        if(! $scope.newPhone) return;

        phones.push($scope.newPhone);
        $scope.newPhone = null;
    };

    /**
     * Removing a Phone Item from the Phones Collection
     */
    $scope.remove = phone => {
        phones.splice(phones.indexOf(phone), 1);
    };
});
