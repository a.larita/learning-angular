// The obligatory app Module..
let app = angular.module('app', []);

app.controller('BaseController', $scope => {
    /**
     * Register $data and $methods property
     *
     * Of course angualar need to know what's defined in $data and $methods
     * so we just loop trough them and add them on the $scope itself.
     *
     * I know what you thinkin it's not the most readable thing
     * in the world but it get the job done at the moment
     *
     * When I progress on this training I will have a
     * good idea how to get around this.
     *
     * @param  {Object} $data
     * @param  {Object} $methods
     * @return {void}
     */
    $scope.bind = ($data, $methods) => {
        for (let $key in Object.assign($data, $methods)) {
            $scope[$key] = $data[$key];
        }
    }
});

// Define a $scope within MainController
app.controller('MainController', $scope => {
    /**
     * Just havi'n some fun a bit trying to get my head around
     * the seperation of concern between methods and data
     */
    let $this = $scope;

    // Here where my data will live in.
    let $data = {
        message: 'Hello!',
        isBusy: false
    };

    // Here where my methods will live in as well.
    let $methods = {
        /**
         * Update Message
         *
         * @param  {String} message
         * @return {void}
         */
        update (message) {
            $this.isBusy = true;

            // Simulating AJAX Request returns call after 2s..
            setTimeout(() => {
                $this.message = message;
                $this.reset();
            }, 2000);
        },

        /**
         * Reset Form
         *
         * @return {void}
         */
        reset () {
            $this.$apply(() => {
                $this.isBusy = false;
                $this.newMessage = '';

                swal("Good job!", "You have just updated the message. Simple Right?!", "success");
            });
        }
    };

    /**
     * Binding $data and $methods, bind() is inherited
     * from it's Parent Controller {BaseController}
     */
    $this.bind($data, $methods);
});
