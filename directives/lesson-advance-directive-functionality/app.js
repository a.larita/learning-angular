'use strict';

(app => {
    app = angular.module('app', []);

    // Define entering directive defaults tot "A"
    app.directive("entering", () => (scope, element, attrs) => {
        element.bind("mouseenter", () => {
            console.log("Mouse has entered the div. Good job!. Do you deserved a pat.");
            element.addClass(attrs.entering);
        });
    });

    // Define leaving directive defaults tot "A"
    app.directive("leaving", () => (scope, element, attrs) => {
        element.bind("mouseleave", () => {
            console.log("Mouse has Leave the div. Now go back again!.");
            element.removeClass(attrs.entering);
        });
    });

})()
