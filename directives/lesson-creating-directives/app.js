'use strict'

let app = angular.module('myApp', []);

// Can be invoked using <my-directive> as an element or
// <div my-directive></div> as an attribute to html tag
app.directive('myDirective', () => {
    return {
        template: '<h1 class="title">My Own Directive</h1>'
    }
});

// invoke the directive from a class name
app.directive('myClassDirective', () => {
    return {
        restrict: 'C',
        template: '<h1 class="subtitle">My Class Directive<h1>'
    }
})

// invoked the directive  from a comment
app.directive('myCommentDirective', () => {
    return {
        restrict: 'M',
        replace: true,
        template: '<h1 class="title is-5">My Comment Directive</h1>'
    }
});
