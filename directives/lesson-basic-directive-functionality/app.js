'use strict';

(app => {
    app = angular.module('app', []);

    // Define entering directive defaults tot "A"
    app.directive("entering", () => (scope, element) => {
        element.bind("mouseenter", () => {
            console.log("Mouse has entered the div. Good job!. Do you deserved a pat.");
        });
    });

    // Define leaving directive defaults tot "A"
    app.directive("leaving", () => (scope, element) => {
        element.bind("mouseleave", () => {
            console.log("Mouse has Leave the div. Now go back again!.");
        });
    });

})()
