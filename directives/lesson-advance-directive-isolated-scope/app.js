let app = angular.module('app', []);

// AppController
app.controller('AppController', $scope => {
    $scope.sendAnEmail = (email, message) => {
        let feedback = `
            You haven sent an email to ${$scope.emailTo} from ${email} with a message "${message}".
        `;   
        
        swal('Email Sent!', feedback, 'success');
    };
});

// attribute mailer Directive
app.directive('mailer', () => {
    return {
        restrict: 'E',

        scope: {
            email: '@',
            emailTo: '=',
            send: '&'
        },

        templateUrl: 'mailer.temp.html',

        link: scope => {
            scope.emails = ['taylor@laravel.com', 'Jeff@laracasts.com', 'EvanYou@vuejs.org'];
            scope.emailTo = scope.emails[0];
            scope.message = '';
        }
    }
});