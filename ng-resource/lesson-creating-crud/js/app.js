let app = angular.module('app', ['ui.router', 'ngResource', 'app.controllers', 'app.services']);

app.config($stateProvider => {
    let $routes = [
        {
            name: 'movies',
            url: 'movies',
            view: 'views/movies.html',
            controller: 'MovieListController'
        },
        {
            name: 'showMovie',
            url: 'movies/:id',
            view: 'views/movie-show.html',
            controller: 'MovieViewController'
        },
        {
            name: 'createMovie',
            url: 'movies/create',
            view: 'views/movie-create.html',
            controller: 'MovieCreateController'
        },
        {
            name: 'editMovie',
            url: 'movies/:id/edit',
            view: 'views/movie-edit.html',
            controller: 'MovieEditController'
        },
    ];


    // Register Routes
    $routes.map($route => $stateProvider.state($route.name, {
        url: $route.url,
        templateUrl: $route.view,
        controller: $route.controller
    }));
})

app.run($state => $state.go('movies'));
