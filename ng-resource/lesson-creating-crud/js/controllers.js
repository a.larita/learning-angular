angular.module('app.controllers', []);

let controllers = {
    MovieListController: function ($scope, $state, Movie) {
        $scope.deleteMovie = $movie => $movie.$delete(() => $scope.loadMovies());

        $scope.loadMovies = () => $scope.movies = Movie.query();

        $scope.loadMovies();
    },

    MovieCreateController: function ($scope, $state, Movie) {
        $scope.movie = new Movie();

        $scope.addMovie = () => $scope.movie.$save(() => $state.go('movies'));
    },

    MovieViewController: function ($scope, $stateParams, Movie) {
        $scope.movie = Movie.get({ id: $stateParams.id });
    },

    MovieEditController: function($scope, $state, $stateParams, Movie) {
        $scope.updateMovie = () => $scope.movie.$update(() => $state.go('movies'));

        $scope.movie = Movie.get({ id: $stateParams.id });
    }
}

// Register all controllers
Object.keys(controllers).map(key => {
    angular.module('app.controllers').controller(key, controllers[key]);
});
