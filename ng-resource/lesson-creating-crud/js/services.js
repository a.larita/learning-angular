let factory = angular.module('app.services', []).factory;

factory('Movie', $resource => $resource('http://api.fakemovies.dev/movies/:id',
    { id: '@id' },
    {
        update: {
            method: 'PUT'
        }
    }
));
